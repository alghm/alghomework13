﻿using System.Text;
using static System.Runtime.InteropServices.JavaScript.JSType;

public class Program
{
    public static void Main(string[] args)
    {
        var hashTable = new HashTable(2000);

        for (int i = 0; i < 2000; i++)
        {
            hashTable.Insert(i, $"Value = {i}");
        }


        int index = -1;
        var indents = new StringBuilder();
        foreach (var item in hashTable.hashTable)
        {
            index++;

            indents.AppendLine(index.ToString());
            indents.AppendLine(" ->" + item.Key);
        }
        Console.WriteLine(indents);

        Console.Read();
    }

    public class HashTable
    {
        private int size;
        public KeyValuePair<int, string>[] hashTable;

        public HashTable(int size)
        {
            this.size = size;
            hashTable = new KeyValuePair<int, string>[size];
        }

        public int HashFunction(int key)
        {
            return key % size;
        }

        public void Insert(int key, string value)
        {
            int hashIndex = HashFunction(key);
            while (hashTable[hashIndex].Key != 0)
            {
                hashIndex = (hashIndex + 1) % size;
            }
            hashTable[hashIndex] = new KeyValuePair<int, string>(key, value);
        }

        public bool Search(int key)
        {
            int hashIndex = HashFunction(key);
            while (hashTable[hashIndex].Key != 0)
            {
                if (hashTable[hashIndex].Key == key)
                {
                    return true;
                }
                hashIndex = (hashIndex + 1) % size;
            }
            return false;
        }
    }
}